#!/bin/bash

cd /home/cicd/cicd-project
echo "showing path==="
echo $PATH
export PATH="/home/cicd/.local/bin:/home/cicd/.nvm/versions/node/v14.20.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin=/home/cicd/.local/bin"
echo "Showing path again===="
echo $PATH	
git pull --rebase
npm install
pm2 restart frontend

